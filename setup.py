#!/usr/bin/env python
import ez_setup
ez_setup.use_setuptools()

from setuptools import setup

import os
from os.path import join

# Release versioning
def get_version():
	"""Runs version.sh and returns the output."""
	cmd = join(os.path.dirname(__file__), 'scripts', 'gitversion.sh')
	pipe = os.popen(cmd)
	version = pipe.read()
	pipe.close()
	return version.strip()

setup(
	name = "python-git",
	description="A python git interface",
	version=get_version(),
	author="David Aguilar",
	author_email="davvid@gmail.com",
	url="http://ugit.sf.net/",
	packages=['git'],
	license="Python",
	long_description="python-git is an interface to git, a distributed version control system.",
)
