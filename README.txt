python-git: Copyright (c) 2008 David Aguilar

The git module is a wrapper around the git command line interface.
Any git command can be called by simply calling a function of
the same name as the git command.

The command's output is returned in a string.

Passing the optional with_status=True keyword
causes the function to returns a tuple of (status, output) instead.

To specify a dashed option, such as "--index", pass the name
of the option as a keyword parameter set to true.

To specify options with dashes in their name, use
underscores.  For example, --patch-with-raw becomes patch_with_raw.
This applies to the command names as well.

	import git

	print git.rev_parse('HEAD')
	print git.diff('--', 'foo', cached=True)
	print git.commit('foo', F='COMMIT_MSG', s=True)
	print git.log('HEAD^..HEAD', patch_with_raw=True, abbrev=4)

Results in the following commands being executed:

	[ 'git', 'rev-parse', 'HEAD' ]
	[ 'git', 'diff', '--cached', '--', 'foo' ]
	[ 'git', 'commit', '-FCOMMIT_MSG', '-s', 'foo' ]
	[ 'git', 'log', '--abbrev=4', '--patch-with-raw', 'HEAD^..HEAD' ]

Example:

>>> git.foo(with_status=True)
(1, "git: 'foo' is not a git-command. See 'git --help'.")


The strings returned by this module strip off trailing whitespace by
default since it simplifies command usage.  To avoid that pass
raw=True into any git.<command> function.

All git functions specified in the split string list below are present
in the git.commands dictionary at import time.  Any that are not
specified are added dynamically as modules import or use them.

For example:
	import git
	print git.foo()

Is perfectly valid but will raise an exception since git will return
exit status 1.  To not raise exceptions, users can set
git.RAISE_EXCEPTIONS=False or pass allow_errors=True to an
individual command:

	print git.foo(allow_errors=True)

would continue and print the standard git error message:

git: 'foo' is not a git-command. See 'git --help'.

Had git actually found a git-foo command, then its output would have
been returned instead (as expected).

This is to allow seamless upgradeability with future as well as
seamless integration with custom, user-defined git commands.

What this means is that the list of git commands included in this
file is not strictly required for the proper execution of this module.
We include the list, though, to provide convenience for users who
use the python console's tab completion facilities.
