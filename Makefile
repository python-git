#! /usr/bin/make -f

prefix=/usr/local
PYTHON=python2.5

PYTHONPATH=$(prefix)/lib/$(PYTHON)/site-packages

all:
	$(PYTHON) setup.py build

install:
	mkdir -p $(prefix)/lib/$(PYTHON)/site-packages
	$(PYTHON) setup.py install --prefix=$(prefix)

uninstall:
	rm -rf $(prefix)

clean:
	rm -rf dist build python_git.egg-info
	find . -name '*.py[co]' | xargs rm -f

release:
	$(PYTHON) setup.py egg_info -RDb "" sdist bdist_egg register upload
